#include <iostream>
#include <math.h>
#include "Camera.h"
#include "Vector.h"


Camera::Camera() {
}

Camera::Camera(const parser::Camera &scene_camera){
    /*
    The u, v, w vectors of the camera have the following meaning:
    – v: up vector
    – w: opposite of gaze vector
    – u: v x w with x representing cross-product
<<<<<<< HEAD
    m = e + -w.distance 
    q = m + l.u + t.v
s = q + su.u - sv.v*/
    //Left, Right, Bottom, Top .
    //x   ,  y   ,   z    , w
	pos = Vector(scene_camera.position);
=======
    m = e + -w.distance
    q = m + l.u + t.v
    s = q + su.u - sv.v*/
    //Left, Right, Bottom, Top .
    //x   ,  y   ,   z    , w
		pos = Vector(scene_camera.position);
>>>>>>> berkant
    gaze = Vector(scene_camera.gaze);
    up = Vector(scene_camera.up);
    near_plane = scene_camera.near_plane;
    near_distance = scene_camera.near_distance;
    image_width = scene_camera.image_width;
    image_height = scene_camera.image_height;
    image_name  = scene_camera.image_name;

<<<<<<< HEAD
    Vector w = (gaze * -1).normalize();

    /*---- u = up x (-gaze) -----*/
    u = up.cross(w).normalize();

    up = w.cross(u);

    /*---- m = pos + gaze.near_distance -------*/
    Vector m = pos + (gaze * near_distance);
=======
		Vector w = (gaze * -1).normalize();
    /*---- u = up x (-gaze) -----*/
    u = up.cross(w).normalize();

		up = w.cross(u);

    /*---- m = pos + gaze.near_distance -------*/
    Vector m = pos + (w * -1 * near_distance);
>>>>>>> berkant

    /*---- q = m + Left.u + Top.up --------*/
    q = m + ((u * near_plane.x) + (up * near_plane.w));

    /*---- Pw = (Right - Left) / Image Width ------*/
    pixel_width = ( near_plane.y - near_plane.x ) / image_width;

    /*---- Ph = (Top - Bottom ) / Image Height -----*/
    pixel_height = (near_plane.w - near_plane.z) / image_height;
}

/****Parameters : i , j -> x, y coordinates of wrt specific pixel ***/
Ray Camera::generateRay(int i,int j) {
<<<<<<< HEAD
    Vector pixel_i_j = q + (u * ((i+0.5)* pixel_width)) - (up * ((j+0.5) * pixel_height)); 
    Vector dir = pixel_i_j - pos;
    Vector normalised = dir.normalize();
    return Ray(pos,normalised); 
}

=======
    Vector pixel_i_j = q + (u * ((i+0.5)* pixel_width)) - (up * ((j+0.5) * pixel_height));
    Vector dir = (pixel_i_j - pos).normalize();
    return Ray(pos,dir);
}
>>>>>>> berkant
