#ifndef CAMERA_H
#define CAMERA_H

#include "Ray.h"
#include "parser.h"

class Camera {
public:
    int image_width,image_height;
	float near_distance,pixel_width,pixel_height;
    
    Vector pos,gaze,up,u,q;
    
    parser::Vec4f near_plane;
    
    std::string image_name;
	
	Camera(const parser::Camera &scene_camera);
	Camera();
    Ray generateRay(int i,int j);
};

#endif 
