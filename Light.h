#ifndef LIGHT_H
#define LIGHT_H

#include "Vector.h"

class Light{
    public:
        Vector position;
        Vector intensity;
        Light(const Vector &position_,const Vector &intensity_) : position(position_), intensity(intensity_){}
        Light(const parser::PointLight &rhs): position(rhs.position), intensity(rhs.intensity){}
        Light(const Light &rhs) : position(rhs.position), intensity(rhs.intensity){}

};



#endif
