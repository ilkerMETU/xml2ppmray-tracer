#ifndef OBJECT_H
#define OBJECT_H

#include "Ray.h"


class Object {
    public:
        Vector ambient_reflectance;
        Vector diffuse_reflectance;
        Vector specular_reflectance;
        Vector mirror_reflectance;
        float phong_exponent;
        virtual float intersect(Ray& ray,float epsilon) = 0;
        virtual Vector getNormal(Vector& point) = 0;

        Object(const parser::Material &material): ambient_reflectance(material.ambient), diffuse_reflectance(material.diffuse),
        specular_reflectance(material.specular), mirror_reflectance(material.mirror),
        phong_exponent(material.phong_exponent){}      
};

#endif
