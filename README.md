# RayTracer

Ray Tracing program that converts XML scenes to PPM images


## Run following command ##

```
    make all
    ./raytacer <name of the scene file>
```