#ifndef RAY_H
#define RAY_H

#include "Vector.h"

class Ray {
	public:
		double t_min;
		double t_max;
		Vector pos;
		Vector dir;
		
        Ray() : pos(Vector()), dir(Vector()) {}
		Ray(Vector &pos_, Vector &dir_): pos(pos_), dir(dir_) {}
		Ray(const Ray& rhs): pos(rhs.pos), dir(rhs.dir){}
		static void print(Ray& ray);
};

#endif
