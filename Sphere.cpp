#include <cmath>
#include <iostream>
#include <math.h>
#include "Sphere.h"



/*---- At^2 + Bt + C - R^2 = 0 ------*/
float Sphere::intersect(Ray& ray,float epsilon) {

    /*---- A = (d).(d) ----*/
    float A = ray.dir.dot(ray.dir);
    /*----- B = 2.(d)(position - center) -----*/
    float B = (ray.dir*2).dot(ray.pos- center); 
    
    /*------ C = (position - center).(position - center) - radius^2 ------*/
    float C = (ray.pos- center).dot(ray.pos-center) - radius*radius;

	float root = B * B - 4 * A * C;
	
    /*if there is no intersection point*/
    if (root < 0) {
		return INFINITY;
	}

	float t1 = (-B + sqrt(root))/(2.f * A);
	float t2 = (-B - sqrt(root))/(2.f * A);

    /*--- if any t is less than 0, thit =  max(t1,t2) -----*/
    if(t1 < 0 || t2 < 0)
    {   
        return std::max(t1,t2);
    }
    /*---- otherwise , thit = min(t1,t2) -----*/
    else
    {
        return std::min(t1,t2);
    }
}

Vector Sphere::getNormal(Vector& point)
{
    return (point-center) / radius;
}

