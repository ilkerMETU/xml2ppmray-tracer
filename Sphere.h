#ifndef SPHERE_H
#define SPHERE_H

#include "Object.h"

class Sphere : public Object
{
	public:
    	float radius;
		Vector center;


		Sphere(const parser::Vec3f &center_ , float radius_,const parser::Material &material): center(center_), radius(radius_) , Object(material){}
		float intersect(Ray& ray,float epsilon);
		Vector getNormal(Vector& point);

};

#endif
