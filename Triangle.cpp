#include <math.h>
#include "Triangle.h"


float getDeterminant(Vector & v1 , Vector & v2, Vector & v3){
    return v1.x * (v2.y * v3.z - v3.y * v2.z) - v2.x * (v1.y* v3.z -  v3.y * v1.z) + v3.x * (v1.y * v2.z- v2.y*v1.z);
}

float Triangle::intersect(Ray& ray,float epsilon)
{   
    // TO DO : IMPLEMENT METHOD : DETERMINANT OPERATIONS ARE INCORRECT!!!!!!!!!!!!!!!
    float beta,gamma,t;

    //APPLY CRAMER'S RULE
    // |A|
    Vector k,l,m;
    k = a-b;
    l = a-c;
    m = a-ray.pos;
    
    
    float dividend_determinant = getDeterminant(k,l,ray.dir);
    
    // DIVIDE BY ZERO
    if(dividend_determinant == 0)
    {
        return INFINITY;
    }

    
       
    // beta = | divider_beta | / |A|
    beta = getDeterminant(m,l,ray.dir) / dividend_determinant;

    // IF BETA > 1 or BETA < 0
    if(beta < -epsilon || beta > 1 + epsilon )
    {
        return INFINITY;
    }

    
    // gamma = | divider_gamma | / |A|
    gamma = getDeterminant(k,m,ray.dir) / dividend_determinant;

    if(gamma < -epsilon || gamma > 1 + epsilon)
    {   
        return INFINITY;
    }
    if(gamma + beta > 1 + epsilon || gamma + beta < -epsilon )
    {
        return INFINITY;
    }


    // t = | t | / |A|
    t = getDeterminant(k,l,m) / dividend_determinant;

    
    //if beta and gamma > 0 and < 1, return intersection parameter t 
    return t;
}

Vector Triangle::getNormal(Vector& point)
{
    return (c-b).cross(a-b).normalize();
}
