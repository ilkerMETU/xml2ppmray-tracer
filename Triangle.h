#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Object.h"



class Triangle : public Object
{
    public:
        Vector a,b,c;
	    Triangle(const parser::Vec3f &a_,const parser::Vec3f &b_,const parser::Vec3f &c_,const parser::Material &material):a(a_),b(b_),c(c_) , Object(material){}
	    float intersect(Ray& ray,float epsilon);
        Vector getNormal(Vector& point);
};


#endif
