#include <iostream>
#include <math.h>
#include "Vector.h"

using namespace std;


float Vector::getMagnitude(){ 
    return sqrt(x*x + y*y + z*z);
}

float Vector::distance_2(Vector const &rhs){
    return (x-rhs.x) * (x-rhs.x) + (y- rhs.y) * ( y - rhs.y) + (z - rhs.z) * (z- rhs.z);
}
Vector Vector::normalize(){
    float mag = getMagnitude();
    return Vector(x/mag, y/mag , z/mag);
}
Vector Vector::cross(Vector const &vect) const {
    return Vector(y*vect.z- vect.y *z , z*vect.x - x * vect.z, x*vect.y - y* vect.x);
}
float Vector::dot(Vector const & vect ) const {
   return x * vect.x + y* vect.y + z * vect.z;
}

void Vector::print_vector(){

   cout << "<" << x << "," << y << "," << z << ">" << endl;
}


Vector Vector::operator+(Vector const & vect) const {
   return Vector(x + vect.x, y + vect.y, z + vect.z);
}

Vector Vector::operator-(Vector const & vect) const {
   return Vector(x - vect.x, y - vect.y, z - vect.z);
}


Vector Vector::operator*(float const scalar) const {
   return Vector(x * scalar, y * scalar, z * scalar);
}

Vector Vector::operator/(float const scalar) const {
   return Vector(x / scalar, y / scalar, z / scalar);
}

 Vector& Vector::operator=(Vector const &vect){
     this -> x = vect.x;
     this -> y = vect.y;
     this -> z = vect.z;

     return *this;
 }






