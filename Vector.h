#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "parser.h"

class Vector{
    
    public:
        float x,y,z;
        Vector(): x(0),y(0),z(0) {}
        Vector(float x_val , float y_val, float z_val) : x(x_val), y(y_val), z(z_val){}
        Vector(const Vector &rhs): x(rhs.x),y(rhs.y), z(rhs.z){} //Copy constructor
		Vector(const parser::Vec3f &rhs): x(rhs.x),y(rhs.y), z(rhs.z){} //Copy constructor

        float getMagnitude ();
        float distance_2(Vector const &rhs);
        Vector normalize();
        Vector cross(Vector const &vect) const ;
        float dot(Vector const & vect ) const ;
        void print_vector();
        Vector& operator=(Vector const &vect) ;
        Vector operator+(Vector const &vect) const;
        Vector operator-(Vector const &vect) const;
        Vector operator*(float const scalar) const;
        Vector operator/(float const scalar) const;

};



#endif