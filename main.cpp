#include <iostream>
#include <math.h>
#include "ppm.h"
#include "Camera.h"
#include "Sphere.h"
#include "Triangle.h"
#include "Light.h"



typedef struct color{
    float R;
    float G;
    float B;

    color(float R , float G , float B)
    {
        this -> R = R;
        this -> G = G;
        this -> B = B;

    }

    color(const parser::Vec3i &rhs)
    {
        this -> R = rhs.x;
        this -> G = rhs.y;
        this -> B = rhs.z;

    }

    color(const color &rhs)
    {
        this -> R = rhs.R;
        this -> G = rhs.G;
        this -> B = rhs.B;

    }
    color& operator+=(const color& rhs)
    {   
        this -> R += rhs.R;
        this -> G += rhs.G;
        this -> B += rhs.B;
        
        return *this;
    }

    color& operator*(const Vector& rhs)
    {   
        this -> R *= rhs.x;
        this -> G *= rhs.y;
        this -> B *= rhs.z;
        
        return *this;
    }
} RGB;

RGB clamp(const RGB& color)
{
    float R,G,B;
    
    R = (color.R > 255) ? 255 : color.R ;
    G = (color.G > 255) ? 255 : color.G ;
    B = (color.B > 255) ? 255 : color.B ;
    
    return RGB(R,G,B);
}


using namespace std;


 
/*********** RAY TRACING ALGORITHM ************/
/*
foreach pixel s:
    compute the viewing ray r (from e to s)
    t min = ∞ , obj = NULL
    foreach object  o:
        if r  intersects o at point x:
            if t < t min:
            tmin = t,  obj = o
    if obj not NULL: // viewing ray intersected  with an object
        pixel color = La // ambient shading is not affected by shadows
        foreach light  l:
            compute the shadow ray s  from x to l
            foreach object  p:
                if s intersects p before the light source:
                    continue the light  loop;  // point is in shadow – no contribution  from this light
                pixel color += Ld + Ls // add diffuse and specular  components for this light  source
    else
        pixel color = color of background (or black)
    
    DO NOT FORGET CLAMPING THE PIXEL VALUE TO [0,255] RANGE  
 AND ROUNDING IT TO NEAREST INTEGER */


RGB diffuse(Vector &diffuse_reflectance,Vector &point_to_light,
Vector &surface_normal,Vector& intensity,float distance_2);

RGB specular(Vector &camera_to_point,float phong_exp,Vector &specular_reflectance,
Vector &point_to_light,Vector &surface_normal,Vector& intensity,float distance_2);

Vector mirror(Vector &surface_normal,Vector &camera_to_point);

RGB ambient(Vector& ambient_reflectance,parser::Vec3f& ambient_light);


RGB TraceRay(Ray &ray,vector<Object*> &objects,vector<Light> &lights,unsigned int depth,
unsigned int max_depth,float ERROR_EPSILON,RGB& background_color,parser::Vec3f& ambient_light);

int main(int argc, char* argv[])
{
    // Create scene from parser.h
    parser::Scene scene;
    
    // Load scene from XML file
    scene.loadFromXml(argv[1]);

    // BACKGROUND COLOR
    RGB background_c(scene.background_color);

    
    //SET ERROR EPSILON
    float ERROR_EPSILON = scene.shadow_ray_epsilon;

    unsigned int max_depth = scene.max_recursion_depth;

    //CREATE VECTOR OF OBJECTS
    vector<Object*> objects;
    

    // Temporary vertex vector
    vector<parser::Vec3f> tmp_vertices = scene.vertex_data;

    //Temporary material vector
    vector<parser::Material> tmp_materials = scene.materials;
   
    
    // DUMMY VARIABLES FOR VERTEX INDICES
    int v_id0,v_id1,v_id2,m_id,c_id = 0;


    vector<Light> lights;
    for(vector<parser::PointLight>::iterator it = scene.point_lights.begin();
    it != scene.point_lights.end();it++ )
    {
        lights.push_back(Light(*(it)));
    }

   //  ADD TRIANGLES
    for(vector<parser::Triangle>::iterator it = scene.triangles.begin();
    it != scene.triangles.end(); it++)
    {   
        v_id0 = it -> indices.v0_id -1 ;
        v_id1 = it -> indices.v1_id -1 ;
        v_id2 = it -> indices.v2_id -1 ;
        m_id = it -> material_id - 1;
        objects.push_back(new Triangle(tmp_vertices.at(v_id0),
        tmp_vertices.at(v_id1),
        tmp_vertices.at(v_id2),
        tmp_materials.at(m_id)));
    }

    // ADD SPHERES
    for(vector<parser::Sphere>::iterator it = scene.spheres.begin();
    it != scene.spheres.end(); it++)
    {   
        m_id = it -> material_id - 1;
        c_id = it -> center_vertex_id - 1;
        objects.push_back(new Sphere(tmp_vertices.at(c_id),
        it -> radius,
        tmp_materials.at(m_id)));
    }


    /*--- INSTEAD OF SINGLE MESH OBJECT ;MULTIPLE TRIANGLE OBJECT IS ADDED !! ---*/
    
    // ADD MESH TRIANGLES
    for(vector<parser::Mesh>::iterator it = scene.meshes.begin();
    it != scene.meshes.end(); it++)
    {   
        
        // number of faces 
        int face_num = it -> faces.size();
        m_id = it -> material_id -1;
        
        // add triangles to the mesh 
        for(int i = 0 ; i < face_num ; i++)
        {
            v_id0 = it -> faces.at(i).v0_id -1 ;
            v_id1 = it -> faces.at(i).v1_id -1 ;
            v_id2 = it -> faces.at(i).v2_id -1 ;

            objects.push_back(new Triangle(tmp_vertices.at(v_id0),
            tmp_vertices.at(v_id1),
            tmp_vertices.at(v_id2),
            tmp_materials.at(m_id)));
        }
    }

    //TRAVERSE EACH CAMERA
    for(vector<parser::Camera>::iterator it = scene.cameras.begin();
    it != scene.cameras.end();it++)
    {
        Camera camera = *it;
        unsigned char* image = new unsigned char [camera.image_width * camera.image_height * 3];
    
        
        //generate ray from given i,j pixel coordinates
        // every pixel contains 3 values
        int height_limit = camera.image_height * 3;
        int width_limit = camera.image_width * 3;
        int height_index;
        for(int j = 0 ; j < height_limit ; j += 3)
        {   
            height_index = camera.image_width * j;
            for(int i = 0 ; i < width_limit ; i += 3)
            {   
                //Trace ray and get color.WHITE OR BACKGORUND_COLOR FOR NOW. WILL BE IMPROVED!
                Ray castedray = camera.generateRay(i/3,j/3);
                RGB color = clamp(TraceRay(castedray,objects,lights,
                0,max_depth,ERROR_EPSILON,background_c,scene.ambient_light));
                image[height_index + i ] = (unsigned char) round(color.R);
                image[height_index + i + 1] = (unsigned char) round(color.G);
                image[height_index + i + 2] = (unsigned char) round(color.B);
                
            }
        }
        write_ppm(camera.image_name.c_str(), image,camera.image_width,camera.image_height);
        
        //CLEAR MEMORY
        delete [] image;
        image = NULL;
    }

    for (std::vector<Object*>::iterator it = objects.begin() ; it != objects.end(); ++it)
    {
        delete (*it);
        *it = NULL;
    } 
    objects.clear();
    lights.clear();
    tmp_materials.clear();
    tmp_vertices.clear();
}


// COLORING ALGORITHM YANLIŞ BÜYÜK İHTİMAL
RGB TraceRay(Ray &ray,vector<Object*> &objects,vector<Light> &lights
,unsigned int depth,unsigned int max_depth,float ERROR_EPSILON
,RGB& background_color,parser::Vec3f &ambient_light)
{
    float t_min = INFINITY;
    Object* intersectObject = NULL;


    /** check nearest intersection */
    for(vector<Object*>::iterator it = objects.begin();
    it != objects.end();it++)
    {   
        float t = (*it) -> intersect(ray,ERROR_EPSILON);

        if(t > 0 && t < t_min)
        {
            t_min = t;
            intersectObject = *it;
        }
    }
    /* IF THERE IS NO INTERSECTED OBJECT */
    if(intersectObject == NULL)
    {   
        return background_color;
    }
    //if there is intersection
    else{
        RGB color(0,0,0);
        
        // add ambient color 
        color += ambient(intersectObject -> ambient_reflectance,ambient_light);
       
        
        //intersection point
        Vector point = ray.pos + (ray.dir * t_min); 
	    
        //surface normal
        Vector normal = intersectObject-> getNormal(point);
     
        //Traverse point lights
        //flag for controlling whethere there is 
        //another object between light and our object or not
        bool is_open;
        for(vector<Light>::iterator it = lights.begin();
        it != lights.end();it++)
        {   
            is_open = true;
            // W_i
            Vector point_to_light = ((*it).position - point);
            
            float distance_2 = point.distance_2((*it).position);
            Vector ray_position = point + (point_to_light * ERROR_EPSILON);
            Ray shadow_ray = Ray(ray_position,point_to_light);
            
            //if light hits object
            //check all objects to see if another object blockades this object
            for(vector<Object*>::iterator it = objects.begin();
            it != objects.end();it++)
            {   
            
                float t_temp = (*it) -> intersect(shadow_ray,ERROR_EPSILON);
                //if there is another object between light and the our object
                if(t_temp > 0 && t_temp < 1)
                {   
                    is_open = false;
                    break;
                }
            }
            // ther eis no object between light and our object
            if(is_open == true)
            {   
                Vector normalized = point_to_light.normalize();
                color += diffuse(intersectObject -> diffuse_reflectance
                ,normalized,normal,(*it).intensity,distance_2);

                color += specular(ray.dir,intersectObject -> phong_exponent
                ,intersectObject -> specular_reflectance,
                normalized,normal,(*it).intensity,distance_2);
            }
        }

        Vector mir_ref = intersectObject -> mirror_reflectance;
          // if recursion level is not reached
        if(depth < max_depth && (mir_ref.x > 0 || mir_ref.y > 0 || mir_ref.z > 0))
        {   
                // color +=  color(new_ray) * K_m 
                Vector new_dir = mirror(normal,ray.dir).normalize();
                Vector position = point + (new_dir * ERROR_EPSILON);
                Ray new_ray = Ray(position,new_dir);
                    
                color += (TraceRay(new_ray,objects,lights,depth + 1,
                max_depth,ERROR_EPSILON,background_color,ambient_light) * mir_ref);

               
        }  

        return color;
    }
}


// Ka * Ia
RGB ambient(Vector &ambient_reflectance,parser::Vec3f& ambient_light)
{   

    float R,G,B;
    
    R = ambient_reflectance.x *  ambient_light.x ;
    G = ambient_reflectance.y *  ambient_light.y ;
    B = ambient_reflectance.z *  ambient_light.z ;


    return RGB(R,G,B);
}

// diffuse reflectance * max(0, light ray direction * surface normal) * (Intensity / d^2)
RGB diffuse(Vector &diffuse_reflectance,Vector &point_to_light,
Vector &surface_normal,Vector& intensity,float distance_2)
{
    float R,G,B;
    //max(0,W_i.n)
    float dot_product = point_to_light.dot(surface_normal);
    if(dot_product <= 0)
    {
        return RGB(0,0,0);
    }
    else{
              
        R = diffuse_reflectance.x * dot_product * intensity.x / distance_2 ;
        G = diffuse_reflectance.y * dot_product * intensity.y / distance_2 ;
        B = diffuse_reflectance.z * dot_product * intensity.z / distance_2 ;

        return  RGB(R,G,B);
    }
}

// specular reflectance * max(0, light ray direction * surface normal)^( phong exponent) * (Intensity / d^2)
RGB specular(Vector &camera_to_point,float phong_exp,Vector &specular_reflectance,
Vector &point_to_light,Vector &surface_normal,Vector& intensity,float distance_2)
{
    float R,G,B;

    //Wi + W0 / || Wi+W0 ||
    Vector point_to_camera = camera_to_point * -1;
    Vector h = (point_to_camera + point_to_light).normalize();
    float dot_product = h.dot(surface_normal);
    
    //max(0,n.h)
    if(dot_product <= 0)
    {
        return RGB(0,0,0);
    }
    else{
        float phonged = pow(dot_product,phong_exp);

        R = specular_reflectance.x * phonged * intensity.x / distance_2 ;
        G = specular_reflectance.y * phonged * intensity.y / distance_2 ;
        B = specular_reflectance.z * phonged * intensity.z / distance_2 ;

        return  RGB(R,G,B);
    }
}


Vector mirror(Vector &surface_normal,Vector &camera_to_point)
{   

    Vector point_to_camera  = (camera_to_point * (-1));

    return (camera_to_point) + ( (surface_normal * 2) * surface_normal.dot(point_to_camera) );
}
